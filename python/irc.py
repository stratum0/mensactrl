#!/usr/bin/env python3

import socket
import string
import client


def printline(nick: str, msg: str, me: bool = False) -> None:
    l = len(nick) + 3
    if me:
        nick = "* %s " % nick
    else:
        nick = "<%s> " % nick
    while msg:
        client.writeline("%s%s" % (nick, msg[:96 - len(nick)]))
        nick = " " * len(nick)
        msg = msg[96 - len(nick):].strip()


HOST = "irc.libera.chat"
PORT = 6667
NICK = "mensadisplay"
IDENT = "mensadisplay"
REALNAME = "MensaDisplay"
readbuffer = b""

s = socket.socket()
s.connect((HOST, PORT))
s.send(f"NICK {NICK}\r\n".encode("utf-8"))
s.send(f"USER {IDENT} {HOST} bla :{REALNAME}\r\n".encode("utf-8"))
s.send(b"JOIN #stratum0\r\n")
while True:
    readbuffer = readbuffer + s.recv(1024)
    temp = readbuffer.split(b"\n")
    readbuffer = temp.pop()

    for line in temp:
        line = line.rstrip()  # string.rstrip(line)
        words = line.split()
        print(words)
        if words[1] == b"PRIVMSG":
            nick = words[0].split(b"!")[0][1:].decode("utf8")
            msg = b" ".join(words[3:])[1:]
            try:
                if msg.startswith(b"\x01ACTION"):
                    # me
                    printline(nick, msg[8:-1].decode("utf8"), True)
                else:
                    printline(nick, msg.decode("utf8"))
            except UnicodeEncodeError:
                pass
        if (words[0] == b"PING"):
            s.send(b"PONG " + words[1] + b"\r\n")
