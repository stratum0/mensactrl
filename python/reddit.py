#!/usr/bin/env python3
# 10x48x5x7 Pixel

import client
import praw
from time import sleep
import os

SERVER = "tcp://localhost:5570"
XOFF = 0
YOFF = 0
TEXT = ""

r = praw.Reddit(
    client_id=os.environ.get("REDDIT_CLIENT_ID"),
    client_secret=os.environ.get("REDDIT_CLIENT_SECRET"),
    user_agent='Stratum0 Braunschweig Mensadisplay Parser')
reddit = ["opensource", "linux", "netsec", "sysadmin", "worldnews", "shittyaskscience", "showerthoughts", "explainlikeimcalvin", "pettyrevenge", "all"]

while True:
    for i in reddit:
        client.writeline("reddit.com/r/" + i)
        for submission in r.subreddit(i).hot(limit=9):
            TEXT = f'{submission.upvote_ratio:5.0f} :: {submission.title}'
            print(TEXT)
            client.writeline(TEXT)
            sleep(10)
