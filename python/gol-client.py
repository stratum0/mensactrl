#!/usr/bin/env python3

import random
import time
from typing import Sequence
import client

SIZE = 1


def rect(x: int, y: int, w: int, h: int, r: int, g: int, b: int) -> None:
    pixels = []
    for i in range(x, x + w):
        for j in range(y, y + h):
            # pixel(i,j,r,g,b)
            pixels.append((i, j, r))
    client.set_pixels(pixels)


def draw(x: int, y: int, v: int) -> None:
    rect(x * SIZE, y * SIZE, SIZE, SIZE, 255 * v, 255 * v, 255 * v)


class State(object):
    def __init__(self, positions: str, x: int, y: int, width: int, height: int):
        self.width = width
        self.height = height
        self.board = [[False] * self.width for row in range(self.height)]

        active_cells = []

        for l, row in enumerate(positions.splitlines()):
            for r, cell in enumerate(row.strip()):
                if cell == 'o':
                    active_cells.append((r, l))

        board = [[False] * width for row in range(height)]

        for active_cell in active_cells:
            board[active_cell[1] + y][active_cell[0] + x] = True

        self.update(board)

    def update(self, new: Sequence[Sequence[bool]]) -> None:
        for y, row in enumerate(new):
            for x, cell in enumerate(row):
                if self.board[y][x] != cell:
                    self.board[y][x] = cell
                    draw(x, y, cell)

    def display(self) -> str:
        output = ''

        for y, row in enumerate(self.board):
            for x, cell in enumerate(row):
                if self.board[y][x]:
                    output += 'o'
                else:
                    output += '.'
            output += '\n'

        return output


class Game(object):
    def __init__(self, state: State, infinite_board: bool = True):
        self.state = state
        self.width = state.width
        self.height = state.height
        self.infinite_board = infinite_board

    def step(self, count: int = 1) -> None:
        for generation in range(count):
            new_board = [[False] * self.width for row in range(self.height)]

            for y, row in enumerate(self.state.board):
                for x, cell in enumerate(row):
                    neighbours = self.neighbours(x, y)
                    previous_state = self.state.board[y][x]
                    should_live = neighbours == 3 or (neighbours == 2 and previous_state == True)
                    new_board[y][x] = should_live

            self.state.update(new_board)

    def neighbours(self, x: int, y: int) -> int:
        count = 0

        for hor in [-1, 0, 1]:
            for ver in [-1, 0, 1]:
                if not hor == ver == 0 and (self.infinite_board == True or (0 <= x + hor < self.width and 0 <= y + ver < self.height)):
                    count += self.state.board[(y + ver) % self.height][(x + hor) % self.width]

        return count

    def display(self) -> str:
        return self.state.display()


glider = """
oo.
o.o
o..
"""

gun = """
........................o...........
......................o.o...........
............oo......oo............oo
...........o...o....oo............oo
oo........o.....o...oo..............
oo........o...o.oo....o.o...........
..........o.....o.......o...........
...........o...o....................
............oo......................
"""

my_game = Game(State(gun,
                     x=20, y=10,
                     width=client.WIDTH // SIZE, height=client.HEIGHT // SIZE)
               )

while True:
    # print my_game.display()
    my_game.step(1)
