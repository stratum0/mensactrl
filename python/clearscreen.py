#!/usr/bin/env python3
import client


def clear(value: int = 0) -> None:
    pixels = bytes([value,] * (client.HEIGHT * client.WIDTH))
    client.blit(0, 0, client.WIDTH, client.HEIGHT, pixels)


if __name__ == "__main__":
    clear()
